﻿using FluentValidation;
using FluentValidation.Results;
using ProyectoBase.COMMON.Entidades;
using ProyectoBase.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ProyectoBase.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private AbstractValidator<T> Validator;
        private ValidationResult ResultadoDeValidacion;
        public GenericRepository(AbstractValidator<T> validator)
        {
            Validator = validator;
            //TODO: Generar la logia de para el acceso la DB y eliminar la excepción
            throw new NotImplementedException("Generar la lógica para el acceso a la BD");
        }
        public string Error { get; private set; }

        public bool Resultado { get; private set; }
        //TODO:Implementar lógica para recuperar todos los elementos de la entidad
        public IQueryable<T> Read => throw new NotImplementedException("Implementar lógica para recuperar todos los elementos de la entidad");

        public virtual T Create(T entidad)
        {
            //TODO: Generar nuevo ID
            //entidad.Id=????
            entidad.FechaHoraCreacion = DateTime.Now;
            entidad.FechaHoraModificacion = DateTime.Now;
            ResultadoDeValidacion = Validator.Validate(entidad);
            if (ResultadoDeValidacion.IsValid)
            {
                try
                {
                    //TODO: Implementar guardado en DB, eliminar excepción y descomentar el código siguiente
                    throw new NotImplementedException("Implementar guardado en DB");
                    //Resultado = true;
                    //Error = null;
                    //return entidad;

                }
                catch (Exception ex)
                {
                    Resultado = false;
                    Error = ex.Message;
                    return null;
                }
            }
            else
            {
                Resultado = false;
                Error = "Entidad no valida: ";
                foreach (var item in ResultadoDeValidacion.Errors)
                {
                    Error += item.ErrorMessage + "; ";
                }
                return null;
            }

        }

        public void CreateAll(IEnumerable<T> entidades)
        {
            try
            {
                bool entidadesValidas = true;
                foreach (var item in entidades)
                {
                    ResultadoDeValidacion = Validator.Validate(item);
                    if (!ResultadoDeValidacion.IsValid)
                    {
                        entidadesValidas = entidadesValidas && false;
                        Error += "Error la entidad con Id: " + item.Id + " presenta los siguientes errores de validación: ";
                        foreach (var err in ResultadoDeValidacion.Errors)
                        {
                            Error += err.ErrorMessage + ", ";
                        }
                        Error += "; ";
                    }
                }
                if (entidadesValidas)
                {
                    //TODO: Implementar lógica para insertar varias entidades a la vez en la BD, eliminar excepción y descomentar el código siguiente
                    throw new NotImplementedException("Implementar lógica para insertar varias entidades a la vez en la BD");
                    //Resultado = true;
                    //Error = "";
                }
                else
                {
                    Resultado = false;
                }
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                //TODO: Implementar eliminación en la BD, eliminar excepción y descomentar el código siguiente
                //int r = ???
                throw new NotImplementedException("Implementar eliminación en la BD");
                //Resultado = true;
                //Error = "";
                //return r > 0;
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
                return false;
            }
        }

        public void DeleteAll(Expression<Func<T, bool>> predicado)
        {
            try
            {
                //TODO: Implementar lógica para eliminar varias entidades en base a una expresión lambda, eliminar excepción y descomentar el código siguiente
                throw new NotImplementedException("Implementar lógica para eliminar varias entidades en base a una expresión lambda");
                //Resultado = true;
                //Error = "";
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
            }

        }

        public IQueryable<T> Query(Expression<Func<T, bool>> predicado)
        {
            try
            {
                //TODO: Implementar la lógica para lla consulta mediante una expresión lambda, eliminar excepción y descomentar el código siguiente
                throw new NotImplementedException("Implementar lógica para implementar la consulta mediante una expresión lambda");
                //Resultado = true;
                //Error = "";
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
                return null;
            }

        }

        public T SearchById(string id)
        {
            try
            {
                //TODO: Implementar la lógica para la búsqueda de un elemento mediante su Id, eliminar excepción y descomentar el código siguiente
                throw new NotImplementedException("Implementar la lógica para la búsqueda de un elemento mediante su Id");
                Resultado = true;
                Error = "";
            }
            catch (Exception ex)
            {
                Resultado = false;
                Error = ex.Message;
                return null;
            }

        }

        public T Update(T entidad)
        {
            entidad.FechaHoraModificacion = DateTime.Now;
            ResultadoDeValidacion = Validator.Validate(entidad);
            if (ResultadoDeValidacion.IsValid)
            {
                try
                {
                    //TODO: Implementar la lógica para la actualización en la BD, eliminar excepción y descomentar el código siguiente
                    throw new NotImplementedException("Implementar la lógica para la actualización en la BD");
                    //int r =???;
                    //Resultado = true;
                    //Error = "";
                    //return r > 0 ? entidad : null;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    Resultado = false;
                    return null;
                }

            }
            else
            {
                Resultado = false;
                Error = "Entidad no valida: ";
                foreach (var item in ResultadoDeValidacion.Errors)
                {
                    Error += item.ErrorMessage + "; ";
                }
                return null;
            }
        }
    }
}
