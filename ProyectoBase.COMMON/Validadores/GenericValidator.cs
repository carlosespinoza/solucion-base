﻿using FluentValidation;
using ProyectoBase.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoBase.COMMON.Validadores
{
    public abstract class GenericValidator<T> : AbstractValidator<T> where T : BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(p => p.Id).NotNull();
            RuleFor(p => p.FechaHoraCreacion).NotNull();
            RuleFor(p => p.FechaHoraModificacion).NotNull();
        }

    }
}
