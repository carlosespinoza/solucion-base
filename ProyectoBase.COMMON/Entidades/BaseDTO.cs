﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoBase.COMMON.Entidades
{
    public abstract class BaseDTO : IDisposable
    {
        //TODO: Cambiar el tipo Id por el deseado
        public String Id { get; set; }
        public DateTime FechaHoraCreacion { get; set; }
        public DateTime FechaHoraModificacion { get; set; }
        public string Etiqueta { get; set; }
        private bool isDisposed;
        public void Dispose()
        {
            if (!isDisposed)
            {
                this.isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }
    }
}
