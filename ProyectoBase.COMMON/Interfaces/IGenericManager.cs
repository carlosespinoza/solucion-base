﻿using ProyectoBase.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoBase.COMMON.Interfaces
{
    public interface IGenericManager<T> where T : BaseDTO
    {
        string Error { get; set; }
        bool Resultado { get; }
        T Crear(T entidad);
        IQueryable<T> ObtenerTodos { get; }
        T Actualizar(T entidad);
        bool Borrar(string id);
        T BuscarPorId(string id);
    }
}
