﻿using ProyectoBase.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ProyectoBase.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T : BaseDTO
    {
        string Error { get; }
        bool Resultado { get; }
        T Create(T entidad);
        IQueryable<T> Read { get; }
        T Update(T entidad);
        bool Delete(string id);
        T SearchById(string id);
        IQueryable<T> Query(Expression<Func<T, bool>> predicado);
        void CreateAll(IEnumerable<T> entidades);
        void DeleteAll(Expression<Func<T, bool>> predicado);
    }
}
