﻿using ProyectoBase.COMMON.Entidades;
using ProyectoBase.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoBase.BIZ
{
    public abstract class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        private IGenericRepository<T> _genericRepository;

        public GenericManager(IGenericRepository<T> genericRepository) => _genericRepository = genericRepository;

        public string Error { get => _genericRepository.Error; set { } }

        public bool Resultado => _genericRepository.Resultado;

        public IQueryable<T> ObtenerTodos => _genericRepository.Read;

        public T Actualizar(T entidad) => _genericRepository.Update(entidad);

        public bool Borrar(string id) => _genericRepository.Delete(id);

        public T BuscarPorId(string id) => _genericRepository.SearchById(id);

        public virtual T Crear(T entidad) => _genericRepository.Create(entidad);
    }
}
